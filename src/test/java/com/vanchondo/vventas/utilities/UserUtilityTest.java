package com.vanchondo.vventas.utilities;

import java.util.ArrayList;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertFalse;

import com.vanchondo.vventas.entities.RoleEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class UserUtilityTest{

    @Test
    public void testIsAdminUsingOneAdminElement (){
        ArrayList<String> roles = new ArrayList<>();
        roles.add(RoleEnum.ADMIN.toString());

        assertTrue("Result should be true", UserUtility.isAdmin(roles));

        roles = new ArrayList<>();
        roles.add(RoleEnum.ADMIN.toString().toLowerCase());

        assertTrue("Result should be true", UserUtility.isAdmin(roles));
    }

    @Test
    public void testIsAdminUsingOneNonAdminElement (){
        ArrayList<String> roles = new ArrayList<>();
        roles.add(RoleEnum.USER.toString());

        assertFalse("Result should be false", UserUtility.isAdmin(roles));
    }

    @Test
    public void testIsAdminUsingMultipleElementsAdminIncluded (){
        ArrayList<String> roles = new ArrayList<>();
        roles.add(RoleEnum.USER.toString());
        roles.add(RoleEnum.ADMIN.toString());

        assertTrue("Result should be true", UserUtility.isAdmin(roles));

        roles = new ArrayList<>();
        roles.add(RoleEnum.ADMIN.toString());
        roles.add(RoleEnum.USER.toString());

        assertTrue("Result should be true", UserUtility.isAdmin(roles));
    }

    @Test
    public void testIsAdminUsingMultipleElementsNonAdminIncluded (){
        ArrayList<String> roles = new ArrayList<>();
        roles.add(RoleEnum.USER.toString());
        roles.add(RoleEnum.USER.toString());

        assertFalse("Result should be false", UserUtility.isAdmin(roles));
    }   @Test
    public void isEmailValidTestValid(){
        String email = "someEmail@company.com";
        assertTrue(email + " is a valid email", UserUtility.isEmailValid(email));
    }

    @Test
    public void isEmailValidTestWithSymbols(){
        String email = "wrong$email@.com";
        assertFalse(email + " is an invalid username", UserUtility.isEmailValid(email));
    }

    @Test
    public void isEmailValidTest(){
        String email = "asd";
        assertFalse(email + " is an invalid username", UserUtility.isEmailValid(email));
    }

    @Test
    public void isEmailValidTestEmpty(){
        assertFalse("Empty is an invalid username", UserUtility.isEmailValid(""));
    }

    @Test
    public void isEmailValidTestNull(){
        assertFalse("null is an invalid username", UserUtility.isEmailValid(null));
    }
}