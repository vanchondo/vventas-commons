package com.vanchondo.vventas.jwt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;



import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

@Component
public class JwtFilter extends GenericFilterBean {

    private List<String> unsecuredUrls;
    private List<String> unsecuredUrlsMethods;
    private String secretKey;

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;
        if (isUnsecuredUrl(request.getRequestURI(), request.getMethod())){
            chain.doFilter(request, response);
        }
        else if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);

            chain.doFilter(req, res);
        } else {
            final String authHeader = request.getHeader("authorization");
            final String authParam = request.getParameter("token");
            if (StringUtils.isEmpty(authParam) && (authHeader == null || !authHeader.startsWith("Bearer "))) {
                throw new AuthenticationException("Missing or invalid Authorization header");
            }
            String token = StringUtils.isEmpty(authHeader) ? authParam : authHeader.substring(7);

            try {
                final Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
                CurrentUser currentUser = new CurrentUser(claims);
                request.setAttribute("currentUser", currentUser);
//                response.addHeader("Access-Control-Expose-Headers", "Authorization");
//                response.addHeader("Authorization", userService.generateToken(currentUser).getToken());
            } catch (final SignatureException e) {
                throw new ServletException("Invalid token");
            }

            chain.doFilter(req, res);
        }

    }

    private boolean isUnsecuredUrl(String path, String requestMethod) throws ServletException{
        if (unsecuredUrls.size() != unsecuredUrlsMethods.size()){
            logger.error("Unsecured url list does not match with methods list");
            throw new ServletException("Unsecured url list does not match with methods list");
        }
        String cleanedUrl = cleanUrl(path);
        for (int i =0; i<unsecuredUrls.size(); i++){
            if (cleanedUrl.equals(unsecuredUrls.get(i)) && requestMethod.equalsIgnoreCase(unsecuredUrlsMethods.get(i))) {
                return true;
            }
        }
        return false;
    }

    private String cleanUrl(String url){
        StringBuilder urlString = new StringBuilder(url);
        if (urlString.length()>0 && urlString.charAt(0) == '/'){
            urlString.deleteCharAt(0);
        }
        if (urlString.length()>0 && urlString.charAt(urlString.length()-1) == '/'){
            urlString.deleteCharAt(urlString.length()-1);
        }
        return urlString.toString();
    }

    @Value("#{'${com.vanchondo.vventas.unsecuredUrlsMethods}'.split(',') ?: ' '}")
    public void setUnsecuredUrlsMethods(List<String> unsecuredUrlsMethods) {
        this.unsecuredUrlsMethods = unsecuredUrlsMethods;
    }

    @Value("#{'${com.vanchondo.vventas.unsecuredUrls}'.split(',') ?: ' '}")
    public void setUnsecuredUrls(List<String> unsecuredUrls) {
        List<String> urls = new ArrayList<>();
        for (String url : unsecuredUrls){
            urls.add(cleanUrl(url));
        }
        this.unsecuredUrls = urls;
    }

    @Value("${com.vanchondo.vventas.secretKey}")
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}

