package com.vanchondo.vventas.jwt;

import java.util.Date;
import java.util.List;

import io.jsonwebtoken.Claims;

public class CurrentUser {
    private String iss;
    private String email;
    private List<String> roles;
    private Date iat;
    private Date exp;
    private String store;

    public CurrentUser(){}

    public CurrentUser(Claims claims){
        this.iss = claims.get("iss").toString();
        this.email = claims.getSubject();
        this.roles = (List<String>) claims.get("roles");
        this.exp = claims.getExpiration();
        this.iat = claims.getIssuedAt();
        this.store = claims.get("store").toString();
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Date getIat() {
        return iat;
    }

    public void setIat(Date iat) {
        this.iat = iat;
    }

    public Date getExp() {
        return exp;
    }

    public void setExp(Date exp) {
        this.exp = exp;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }
}
