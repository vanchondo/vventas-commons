package com.vanchondo.vventas.utilities;

import java.util.List;

import com.vanchondo.vventas.entities.RoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class UserUtility {
    private final static Logger logger = LoggerFactory.getLogger(UserUtility.class);

    public static boolean isEmailValid(final String email){
        if (email!=null) {
            return email.matches("[A-Za-z0-9_\\.]{4,100}@[A-Za-z0-9_]+\\.[A-Z-a-z]{2,50}");
        }
        logger.debug(email + " not valid.");
        return false;
    }

    public static boolean isAdmin (List<String> roles){
        return roles.stream().anyMatch(x -> x.equalsIgnoreCase(RoleEnum.ADMIN.toString()));
    }
}
